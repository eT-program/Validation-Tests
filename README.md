# Validation Tests

Repository containing inputs and outputs validating the results
of [eT](https://etprogram.org/) against other programs, like:
- [Psi4](https://psicode.org/psi4manual/master/)

for example.
